const express = require('express');
const app = express()

app.use("/chi/", express.static(__dirname + "/game"))
app.use("/chi/*", express.static(__dirname + "/game"))

app.use(function(req, res) {
  console.log("/chi" + req.url)
  res.redirect("/chi" + req.url)
})


app.listen(443)
