/* 
  ▒█▀▀█ █▀▀█ █▀▀█ █   █ █▀▀ █▀▀ █▀▀█ ▒█▀▀█ █▀▀█ █▀▀▄ █▀▀▀ 
  ▒█▀▀▄ █▄▄▀ █  █ █▄█▄█ ▀▀█ █▀▀ █▄▄▀ ▒█▄▄█ █  █ █  █ █ ▀█ 
  ▒█▄▄█ ▀ ▀▀ ▀▀▀▀  ▀ ▀  ▀▀▀ ▀▀▀ ▀ ▀▀ ▒█    ▀▀▀▀ ▀  ▀ ▀▀▀▀ 
 
  Copyright George Fischer 2018

  # browser.dart
    Contains helper functions for window management
  
*/